import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:login_firebase/Controller/LoginController.dart';
import 'package:login_firebase/View/LoginPage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomePage> {
  final LoginController controller = Get.put(LoginController()); //Get.find();

  @override
  void initState() {
    super.initState();

    getUser();
  }

  getUser() async {
    await controller.getUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("User Info"),
        ),
        body: SingleChildScrollView(
          child: Padding(
              padding: EdgeInsets.only(left: 32, right: 32),
              child: Obx(() {
                return Form(
                    key: controller.formKey,
                    child: Center(
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(controller.userInfo.value.name ?? '',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 32)),
                              SizedBox(height: 8),
                              TextFormField(
                                enabled: !controller.loginProcess.value,
                                controller: controller.emailTextController,
                                decoration: InputDecoration(
                                    icon: Icon(Icons.person),
                                    labelText: "Email"),
                                validator: (String value) =>
                                    GetUtils.isEmail(value)
                                        ? null
                                        : "Please enter a valid email",
                              ),
                              SizedBox(height: 8),
                              TextFormField(
                                enabled: !controller.loginProcess.value,
                                controller: controller.nameTextController,
                                decoration: InputDecoration(
                                    icon: Icon(Icons.person),
                                    labelText: "User name"),
                                validator: (String value) => value.isNotEmpty
                                    ? null
                                    : "Tên không được bỏ trống",
                              ),
                              SizedBox(height: 32),
                              Material(
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(30),
                                color: controller.loginProcess.value
                                    ? Theme.of(context).disabledColor
                                    : Theme.of(context).primaryColor,
                                child: MaterialButton(
                                  minWidth: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                  onPressed: () async {
                                    FocusScope.of(context).unfocus();
                                    if (controller.formKey.currentState
                                        .validate()) {
                                      final result =
                                          await controller.updateInfo(
                                              name: controller
                                                  .nameTextController.text,
                                              email: controller
                                                  .emailTextController.text);
                                      Get.defaultDialog(
                                          title: "Thông báo",
                                          middleText: result
                                              ? 'Cập nhật thành công'
                                              : controller.error);
                                    }
                                  },
                                  child: Text(
                                    "Update",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              SizedBox(height: 50),
                              TextButton(
                                  onPressed: () {
                                    controller.logout();
                                    Get.off(LoginPage());
                                  },
                                  child: Text('Logout'))
                            ]),
                      ),
                    ));
              })),
        ));
  }
}
