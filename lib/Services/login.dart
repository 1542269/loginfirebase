import 'package:firebase_auth/firebase_auth.dart';
import 'package:login_firebase/Controller/LoginController.dart';
import 'package:login_firebase/Model/User.dart';

class FireBaseLogin {
  static Future<bool> login({String email, String password}) async {
    try {
      await auth.signInWithEmailAndPassword(email: email, password: password);
      // await db.child('users').child(user.uid).set({
      //   'email': user.email,
      //   'name': 'Nguyễn Bảo Thanh'
      // });
      return true;
    } catch (e) {
      throw e.toString();
    }
  }

  static Future<bool> update({String email, String name}) async {
    try {
      await db
          .child('users')
          .child(auth.currentUser.uid)
          .update({'name': name, 'email': email});
      return true;
    } catch (e) {
      throw e.toString();
    }
  }

  static Future<UserModel> getUser() async {
    try {
      final data = await db.child('users').child(auth.currentUser.uid).get();

      return UserModel.fromJson(Map<String, dynamic>.from(data.value));
    } catch (e) {
      throw e.toString();
    }
  }
}
