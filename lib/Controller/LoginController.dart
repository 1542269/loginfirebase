import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:login_firebase/Model/User.dart';
import 'package:login_firebase/Services/login.dart';

final FirebaseAuth auth = FirebaseAuth.instance;
final DatabaseReference db = FirebaseDatabase.instance.reference();

class LoginController extends GetxController {
  var emailTextController = TextEditingController(text: "");
  var nameTextController = TextEditingController(text: "");

  final formKey = GlobalKey<FormState>();
  var userInfo = UserModel().obs;
  var loginProcess = false.obs;
  var error = '';

  @override
  void onInit() {
    super.onInit();

    db.child('users').onValue.listen((event) {
      final userData =
          Map<String, dynamic>.from(event.snapshot.value[auth.currentUser.uid]);
      final user = UserModel.fromJson(userData);
      userInfo(user);
      emailTextController.text = user.email ?? '';
      nameTextController.text = user.name ?? '';
    });
  }

  getUser() async {
    final user = await FireBaseLogin.getUser();
    userInfo(user);
    emailTextController.text = user.email ?? '';
    nameTextController.text = user.name ?? '';
  }

  Future<bool> login({String email, String password}) async {
    bool success;
    try {
      loginProcess(true);
      await FireBaseLogin.login(email: email, password: password);
      await getUser();
      success = true;
    } catch (e) {
      success = false;
      error = e.toString();
    } finally {
      loginProcess(false);
    }
    return success;
  }

  Future<bool> updateInfo({String email, String name}) async {
    bool success;
    try {
      loginProcess(true);
      await FireBaseLogin.update(name: name, email: email);
      await auth.currentUser.updateEmail(email);
      success = true;
    } catch (e) {
      success = false;
      error = e.toString();
    } finally {
      loginProcess(false);
    }
    return success;
  }

  Future logout() async {
    await auth.signOut();
  }
}
